#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/wait.h>

int main()
{
    // declare variable
    char url[100];
    char filename[100];
    char command[500];
    time_t t;
    struct tm *lt;

    while (1)
    {
        // get the timestamp for the folder name
        time(&t);
        lt = localtime(&t);
        char foldername[100];
        strftime(foldername, sizeof(foldername), "%Y-%m-%d_%H:%M:%S", lt);

        // create directory
        mkdir(foldername, 0777);

        // download image
        for (int i = 1; i <= 15; i++)
        {
            // get the current second
            time(&t);
            lt = localtime(&t);
            int current_second = lt->tm_sec;

            // calculate width and height (square)
            int px = (current_second % 1000) + 50;

            // random image URL
            sprintf(url, "https://picsum.photos/%d", px);

            // get timestamp for file name
            strftime(filename, sizeof(filename), "%Y-%m-%d_%H:%M:%S.jpg", lt);

            // allocated image into folder
            char filepath[200];
            sprintf(filepath, "%s/%s", foldername, filename);

            // command to download the image
            sprintf(command, "wget -q %s -O %s", url, filepath);

            // create a child process to execute the command
            pid_t pid = fork();
            if (pid == 0)
            {
                // child process
                execlp("wget", "wget", "-q", url, "-O", filepath, NULL);
                exit(0);
            }
            else if (pid > 0)
            {
                // parent process
                int status;
                waitpid(pid, &status, 0);
            }
            else
            {
                // error
                fprintf(stderr, "fork() failed\n");
                exit(1);
            }
            // delay 5 seconds for next image
            sleep(5);
        }
        // zip the folder
        sprintf(command, "zip -r %s.zip %s", foldername, foldername);
        pid_t pid = fork();

        if (pid == 0)
        {
            // child process
            execlp("sh", "sh", "-c", command, NULL);
            exit(EXIT_FAILURE);
        }
        else if (pid > 0)
        {
            // parent process
            int status;
            waitpid(pid, &status, 0);

            if (WIFEXITED(status) && WEXITSTATUS(status) == 0)
                printf("Folder %s has been zipped.\n", foldername);
            else
                printf("Failed to zip folder %s.\n", foldername);

            // remove the original folder
            sprintf(command, "rm -rf %s", foldername);
            pid = fork();

            if (pid == 0)
            {
                // child process
                execlp("sh", "sh", "-c", command, NULL);
                exit(EXIT_FAILURE);
            }
            else if (pid > 0)
            {
                // parent process
                int status;
                waitpid(pid, &status, 0);

                if (WIFEXITED(status) && WEXITSTATUS(status) == 0)
                    printf("Folder %s has been removed.\n", foldername);
                else
                    printf("Failed to remove folder %s.\n", foldername);
            }
            else
                // error while forking
                printf("Failed.\n");
        }
        else
            // error while forking
            printf("Failed.\n");

        // delay 30 seconds for next folder
        sleep(30);
    }

    return 0;
}
