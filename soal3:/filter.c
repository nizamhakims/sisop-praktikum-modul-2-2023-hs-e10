#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <string.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>

#define PATH "/home/sekarambar/modul2/players/"

int main() {

    // a

    // Download file database pemain bola
    pid_t pid = fork();
    if (pid == 0) {
        char *args[] = {"wget", "-O", "players.zip", "https://drive.google.com/uc?id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF&export=download", NULL};
        execvp(args[0], args);
        exit(EXIT_FAILURE);
    } else {
        wait(NULL);
    }

    // Extract players.zip
    pid = fork();
    if (pid == 0) {
        char *args[] = {"unzip", "players.zip", NULL};
        execvp(args[0], args);
        exit(EXIT_FAILURE);
    } else {
        wait(NULL);
    }

    // Remove players.zip
    pid = fork();
    if (pid == 0) {
        char *args[] = {"rm", "players.zip", NULL};
        execvp(args[0], args);
        exit(EXIT_FAILURE);
    } else {
        wait(NULL);
    }




    // b

    pid_t child_pid;
    int status;
    DIR *dir;
    struct dirent *ent;
    char *path = "/home/sekarambar/modul2/players/";
    char filename[256];

    // Fork child process
    child_pid = fork();

    if (child_pid == -1) {
        perror("Error pada proses forking child");
        exit(EXIT_FAILURE);
    } else if (child_pid == 0) {
        // Child process

        // Membuka directory
        dir = opendir(path);

        if (dir == NULL) {
            perror("Error pada saat opening directory");
            exit(EXIT_FAILURE);
        }

        // Melakukan looping pada directory
        while ((ent = readdir(dir)) != NULL) {
            // Mengecek jika file merupakan regular file (bukan direktori atau file khusus)
            if (ent->d_type == DT_REG) {
                // Construct filename
                strcpy(filename, path);
                strcat(filename, ent->d_name);

                // Melakukan pengecekan jika filename mengandung kata "ManUtd"
                if (strstr(ent->d_name, "ManUtd") == NULL) {
                    // Remove file
                    if (unlink(filename) == -1) {
                        perror("Error pada saat penghapusan file");
                    }
                }
            }
        }

        // Close directory
        closedir(dir);

    }


    // c

      chdir("/home/sekarambar/modul2/players/");

  int fork0 = fork();

  //PROSES PEMBUATAN FOLDER :

  //Proses 1
  //buat folder Kiper
  if (fork0 == 0){
    char *argv[] = {"mkdir","-p","Kiper",NULL};
    execv("/bin/mkdir", argv);
  }

  else{
    while ((wait(&status)) > 0);

    //Proses 2
    //buat folder Bek
    int fork1 = fork();
    if (fork1 == 0)
    {
      sleep(3);
      char *argv[] = {"mkdir", "-p", "Bek", NULL};
      execv("/bin/mkdir", argv);
    }

    //Proses 3
    //buat folder Gelandang
    int fork2 = fork();
    if (fork2 == 0)
    {
      sleep(3);
      char *argv[] = {"mkdir", "-p", "Gelandang", NULL};
      execv("/bin/mkdir", argv);
    }

    //Proses 4
    //buat folder Penyerang
    int fork3 = fork();
    if (fork3 == 0)
    {
      sleep(3);
      char *argv[] = {"mkdir", "-p", "Penyerang", NULL};
      execv("/bin/mkdir", argv);
    }
        //PROSES PEMINDAHAN FILE :
        //Proses 1
        //memindahkan Kiper
        while ((wait(&status)) > 0);
        int fork4 = fork();
        if (fork4 == 0)
        {
          char *argv[] = {"find", "/home/sekarambar/modul2/players/", "-name", "*Kiper*", "-exec", "mv", "-t", "/home/sekarambar/modul2/players/Kiper/", "{}", "+", NULL};
          execv("/bin/find", argv);
        }

        //memindahkan Bek
        //Proses 2
        while ((wait(&status)) > 0);
        int fork5 = fork();
        if (fork5 == 0)
        {
          char *argv[] = {"find", "/home/sekarambar/modul2/players/", "-name", "*Bek*", "-exec", "mv", "-t", "/home/sekarambar/modul2/players/Bek/", "{}", "+", NULL};
          execv("/bin/find", argv);
        }

        //Proses 3
        //memindahkan Gelandang
        while ((wait(&status)) > 0);
        int fork6 = fork();
        if (fork6 == 0)
        {
          char *argv[] = {"find", "/home/sekarambar/modul2/players/", "-name", "*Gelandang*", "-exec", "mv", "-t", "/home/sekarambar/modul2/players/Gelandang/", "{}", "+", NULL};
          execv("/bin/find", argv);
        }

        //Proses 4
        //memindahkan Penyerang
        else
        {
          while ((wait(&status)) > 0);
          int fork7 = fork();
          if (fork7 == 0)
          {
            char *argv[] = {"find", "/home/sekarambar/modul2/players/", "-name", "*Penyerang*", "-exec", "mv", "-t", "/home/sekarambar/modul2/players/Penyerang/", "{}", "+", NULL};
            execv("/bin/find", argv);
          }
        }
  }

    return 0;
}
