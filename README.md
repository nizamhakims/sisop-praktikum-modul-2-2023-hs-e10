[](url)soal 1

    a. -download zip file menggunakan system command wget kemudian simpan zip file dengan nama x.zip
       -buat folder temp untuk menyimpan file setelah x.zip di unzip
       -buat fungsi unzip() menggunakan fork dengan child process berisi execvp("unzip", "unzip", namaZip,"-d", namaFolder, NULL)
        dengan namaZip = x.file dan namaFolder = temp kemudian gunakan wait untuk menunggu child process selesai sebelum melanjutkan
        parent process
       -unzip(x.zip)
    
    b. -gunakan system command untuk sort file jpg yang ada di dalam folder temp secara random(sort -R) 
        kemudian tampilkan menggunakan ls

    c. -buat 3 folder (HewanDarat, HewanAmphibi, HewanAir)
       -gunakan system command mv untuk memindahkan file jpg di dalam folder temp dengan ketentuan 
        wildcard *darat* -> HewanDarat
        wildcard *amphibi* -> HewanAmphibi
        wildcard *air* -> HewanAir
       -gunakan system command rmdir untuk menghapus folder temp karena sudah kosong

    d. -buat fungsi zip() menggunakan fork dengan child process berisi execvp("zip", "zip", "-r", namaZip, namaFolder, NULL)
        dengan namaZip = namaFolder + ".zip" dan namaFolder adalah parameter yang dimasukkan ke dalam fungsi 
        (HewanDarat, HewanAmphibi, HewanAir) kemudian gunakan wait untuk menunggu child process selesai 
        sebelum melanjutkan parent process
       -zip(HewanDarat)
       -zip(HewanAmphibi)
       -zip(HewanAir)
       -gunakan system command rm untuk menghapus HewanDarat, HewanAmphibi, HewanAir, dan x.zip

output pada terminal <br />
![](Dokumentasi/output_no_1.png)

isi zip <br />
![](Dokumentasi/hewan_darat.png)
![](Dokumentasi/hewan_air.png)
![](Dokumentasi/hewan_amphibi.png)

soal 2

1. Folder 
- ambil waktu timestamp untuk format nama folder ([YYYY-mm-dd_HH:mm:ss]).
- buat folder dengan jeda 30 detik setiap folder.

2. Download gambar  
- buat perulangan dengan batas 15 gambar untuk setiap isi folder. 
- dapatkan waktu timestamp ([YYYY-mm-dd_HH:mm:ss]) untuk format penamaan setiap gamabr.
- unduh gambar dari link : "https://picsum.photos/%d", dimana %d untuk masukan ukuran gambar yaitu persegi sebesar (t%1000)+50 piksel. 
- jeda unduhan setiap gambar 5 detik.

3. Zip dan Remove
- Zip folder yang terisi 15 gambar unduhan 
- hapus original folder

![](Dokumentasi/2.1.jpg)
![](Dokumentasi/2.2.jpg)
![](Dokumentasi/2.3.jpg)

kesulitan :
- belum overlapping dan tidak ada killer



soal 3

    a. Pertama-tama, Program filter.c akan mengunduh file yang berisikan database para pemain bola. 
       Kemudian dalam program yang sama diminta dapat melakukan extract “players.zip”. 
       Lalu hapus file zip tersebut agar tidak memenuhi komputer Ten Hag.
       --> Download database menggunakan fungsi wget, -o untuk menentukan nama file(players.zip). 
       Kemudian menggunakan unzip untuk mengekstrak zipnya. Karena pada soal diminta untuk me-remove 
       file yang sudah diekstrak, maka menggunakan fungsi rm.
    
    b. Dikarenakan database yang diunduh masih data mentah. Maka bantulah Ten Hag untuk menghapus 
       semua pemain yang bukan dari Manchester United yang ada di directory.
       --> Langkah awal, melakukan open directory dari pathnya ("/home/sekarambar/modul2/players/"). Kemudian melakukan 
       looping pada directory, yang pertama mengecek jika file merupakan regular file (bukan direktori atau file khusus), 
       lalu construct filename. Selanjutnya mengecek jika filename mengandung kata "ManUtd", lalu 
       menghapus file yang tidak mengandung kata “MaunUtd”

    c. Setelah mengetahui nama-nama pemain Manchester United, Ten Hag perlu untuk mengkategorikan pemain tersebut 
       sesuai dengan posisi mereka dalam waktu bersamaan dengan 4 proses yang berbeda. Untuk kategori folder akan 
       menjadi 4 yaitu Kiper, Bek, Gelandang, dan Penyerang.
       --> Pertama-tama proses pembuatan folder dengan fungsi mkdir. –p untuk memberi penamaan Kiper, Bek, Gelandang, 
       dan Penyerang. Lalu untuk memindahkannya, gunakan fungsi find untuk mencari nama-nama yang memiliki unsur 
       Kiper, Bek, Gelandang, dan Penyerang , kemudian dipindahkan dengan fungsi mv ke folder masing-masing.

![](Dokumentasi/d1.png)
![](Dokumentasi/d2.png)

Bek berjumlah 8 <br />
![](Dokumentasi/bek.png)

Gelandang berjumlah 7 <br />
![](Dokumentasi/gelandang.png)

Penyerang berjumlah 5 <br />
![](Dokumentasi/penyerang.png)

Kiper berjumlah 2 <br />
![](Dokumentasi/kiper.png)

kesulitan :
- poin d


soal 4

kesulitan : semua
