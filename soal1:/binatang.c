#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>

void unzip(char namaZip[]){
  pid_t child_id;

  child_id = fork();

  if (child_id == -1) {
    exit(EXIT_FAILURE);
  }
  
  if (child_id == 0) {

    char namaFolder[] = "temp";
    char *argv[] = {"unzip", namaZip,"-d", namaFolder, NULL};
    execvp("unzip", argv);

  }
  int status;
  wait(&status);
}

void zip(char namaFolder[]){
  pid_t child_id;

  child_id = fork();

  if (child_id == -1) {
    exit(EXIT_FAILURE);
  }
  
  if (child_id == 0) {
    
    char namaZip[15];
    strcpy(namaZip, namaFolder);
    strcat(namaZip, ".zip");

    char *argv[] = {"zip", "-r", namaZip, namaFolder, NULL};
    execvp("zip", argv);

  }
  int status;
  wait(&status);
}

int main() {
  //1a
  system("wget -O x.zip 'https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq'");
  system("mkdir temp");
  unzip("x.zip");

  //1b
  printf("Urutan Penjagaan Hewan: \n");
  system("ls /home/nizamhakims/workspace/temp | sort -R ");

  //1c
  system("mkdir HewanDarat HewanAmphibi HewanAir");
  system("for f in /home/nizamhakims/workspace/temp/*darat*; do mv $f /home/nizamhakims/workspace/HewanDarat; done");
  system("for f in /home/nizamhakims/workspace/temp/*air*; do mv $f /home/nizamhakims/workspace/HewanAir; done");
  system("for f in /home/nizamhakims/workspace/temp/*amphibi*; do mv $f /home/nizamhakims/workspace/HewanAmphibi; done");
  system("rmdir temp");
  
  //1d
  zip("HewanAir");
  zip("HewanDarat");
  zip("HewanAmphibi");
  system("rm -r HewanDarat HewanAir HewanAmphibi x.zip");
}